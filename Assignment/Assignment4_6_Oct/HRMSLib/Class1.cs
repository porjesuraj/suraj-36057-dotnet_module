﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace HRMSLib
{
   public class Date
    {
        #region Question
        /*Write class Date.cs with following members:
int day; int month; int year;
Provide following functionality:
Default constructor
Parameterized constructor
Properties: Day (1 -31 allowed also consider leap year),
Month (1 -12 allowed) and
Year (Consider only 20th & 21st century)
string ToString(); override method to return data of object in string format dd/MM/yyyy.
Also provide a static member function that returns difference between two date objects in number of
years.
git commit –m “assignment 4.1 completed”
      */
        #endregion

        #region Data members
        private int day;

        private int month;

        private int year;
        #endregion

        #region Constructors
        public Date()
        {
            this._day = DateTime.Now.Day;
            this._month = DateTime.Now.Month;
            this._year = DateTime.Now.Year;

        }

        public Date(int day, int month, int year)
        {
            this._day = day;
            this._month = month;
            this._year = year;
        }
        #endregion


        #region getter and setter
        public int _year
        {
            get { return year; }
            set
            {
                if(value >= 1900 && value <= 2100)
               
                year = value;
            
            }
        }

        public int _month
        {
            get { return month; }
            set
            {
                if (value >= 0 && value <= 12)
                {
                    this.month = value;
                }
                else
                {
                    Console.WriteLine("incorrect month value, it should be between 1 and 12 ");
                }
                   
            }
        }

        public int _day
        {
            get { return this.day; }
            set 
            { 
                if(value >= 1 && value <= 31)
                {
                    this.day = value;
                }
                else
                {
                    Console.WriteLine("incorrect day value, it should be between 1 and 31 ");
                }
                
                
            }
        }
        #endregion

        #region member function
        public string Date_Detail()
        {
            return this._day.ToString() + "/" + this._month.ToString() + "/" + this._year.ToString();
        }



        public static int DifferenceOfDates(Date date1, Date date2)
        {

            int diff = date1.year - date2.year;

           // int AddedMonths = date1.month + date2.month;
           // int AddedDays = date1.day + date2.day;
            //if (AddedMonths >= 12 )
            //{
            //    diff = diff + 1;
            //}
            //else if (AddedMonths >= 24)
            //{
            //    diff = diff + 2;
            //}
            //
            return Math.Abs(diff);
        }
        #endregion







    }

    public class Person:Date
    {

        #region Question
        /*Write a class Person.cs in HRMSLib project with following members:
string _name; bool _gender; Date _birth;
string _address;
Provide following functionality:
Default constructor
Parameterized constructor
Properties: Name, Gender, Birth, Address and Age (Read Only)
string ToString(); override method to return data of object in string format.
Hint: Class will contain Date object for birth and Age will be calculculated using static method in
Date class.
git commit –m “assignment 4.2 completed”
         */
        #endregion

        #region data member
        private string _name;

        private bool _gender;

        private string _address;

        private Date _birth;

        private string  _EmailId;

        


        #endregion

        #region Constructor
        public Person():base()
        {
            this.name = "";
            this.gender = true;
            this.address = "";
            this.EmailId = "";
            this.birth = new Date(1,1,1970); 
           
        }

        public Person(string Name, bool gender,string email, string add,Date date)
        {
            this.name = Name;
            this.gender = gender;
            this.EmailId = email;
            this.address = add;
            this.birth = date;
           

        }

        public int GetAge(Date birth )
        {
            Date today = new Date();

           return  Date.DifferenceOfDates(birth , today);
           
        }

        #endregion



        #region getter and setter
      
        public Date birth
        {
            get { return _birth; }
            set { _birth = value; }
        }
      

        public string address
        {
            get { return _address; }
            set { _address = value; }
        }


        public bool gender
        {
            get
            {
  
            return _gender;

            }
            set { _gender = value; }
        }

        public string name
        {
            get { return _name; }
            set { _name = value; }
        }

        public string EmailId
        {
            get { return _EmailId; }
            set { _EmailId = value; }
        }
        #endregion


        #region member function
        public void AcceptPersonDetail()
        {
            Console.WriteLine("please enter name");
            this.name = Console.ReadLine();

            Console.WriteLine("do you want to enter your gender ? type true if yes, or false  ");
            this.gender = Convert.ToBoolean(Console.ReadLine());
            Console.WriteLine("please enter address = ");
            this.address = Console.ReadLine();

            Console.WriteLine("please enter employee birthdate in day month year format");
            int day = Convert.ToInt32(Console.ReadLine());
            int month = Convert.ToInt32(Console.ReadLine());
            int year = Convert.ToInt32(Console.ReadLine());

            this.birth = GetBirthDate(day, month, year);

            Console.WriteLine("please enter email address ");
            this.EmailId = Console.ReadLine();

        }
        public string GetPersonDetails()
        {
            return " Name = " + this.name + " " + "  gender given" + this.gender.ToString() + "  address" + this.address +
               "birth date " + "" + this.Date_Detail() + " Email = " + this.EmailId + "Age = " + this.GetAge(this.birth).ToString() + "\n";

        }

        public Date GetBirthDate(int day, int month, int year)
        {
            return new Date(day, month, year);
        }
        #endregion






    }

    public enum EmployeeTypes
     {
        #region Question
        /*Write a enum EmployeeTypes.cs in HRMSLib project with following members:
         Trainee, Permanent, Temporary
         git commit –m “assignment 4.3 completed”
         */
        #endregion

        Trainee,
        Permanent,
        Temporary
    }

    public class Employee:Person 
    {
        #region problem statement
        /*Write a class Employee.cs in HRMSLib project with following members:
          int _id; (Auto Generated)
          double _salary; double _hra (40% of salary); double _da (10% of salary);
          string _designation; enum EmployeeType _empType;
          Provide following functionality:
          Default constructor
          Parameterized constructor
          Properties: Id, Salary, Designation, EmployeeType and NetSalary (Read Only & virtual)
          string ToString(); override method to return data of object in string format.
          Hint: class must be inherited from Person class. Use static count for auto generating id.
          Netsalary = Salary + HRA + DA
          git commit –m “assignment 4.4 completed”
         */
        #endregion

        #region data member

        private static int count = 0;
       
        private double _salary;

        private double _hra;

        private double _da;
        private string _designation;

        private EmployeeTypes _empTypes;

        private int _id;


        private string _Passcode;

        private int _DeptNo;


        private Date _Hiredate;



       

        #endregion


        #region Constructor
        public Employee()
        {
            this._id = ++count;
            this._salary = 1.0;
            this.hra = this._salary  * 0.4;
            this.da = this._salary * 0.1;
            this._designation = "manager";
            this._empTypes = EmployeeTypes.Permanent;
            this.DeptNo = 1;
            this.Hiredate = new Date(1, 1, 2020);
            this.birth = new Date(1, 1, 1971);

        }

        public Employee(double salary, string designation, EmployeeTypes emptype,string passcode,int deptno,Date hiredate, string Name, bool gender,string email, string add, Date date)
            : base(Name, gender,email, add,date)
        {
            this._id = ++count;
            this._salary = salary;
            this.hra = 0.4 * salary;
            this.da = 0.1 * salary;
            this._designation = designation;
            this._empTypes = emptype;
            this.Passcode = passcode;
            this.DeptNo = deptno;
            this.Hiredate = hiredate;
         
        }

        #endregion



        #region getter and setter

        public virtual int id
        {
            get { return _id; }
           
        }

        public virtual EmployeeTypes empType
        {
            get { return _empTypes; }
            set { _empTypes = value; }
        }

        public double da
        {
            get { return _da; }
            set { _da = value; }
        }

        public double hra
        {
            get { return _hra; }
            set { _hra = value; }
        }


        public virtual string designation
        {
            get { return _designation; }
           set { _designation = value; }
        }


        public virtual double salary
        {
            get { return _salary; }
            set { _salary = value; }
        }

        public Date Hiredate
        {
            get { return _Hiredate; }
            set { _Hiredate = value; }
        }

        public int DeptNo
        {
            get { return _DeptNo; }
            set { _DeptNo = value; }
        }



        public string Passcode
        {
            get { return _Passcode; }
            set { _Passcode = value; }
        }



        #endregion


        #region member Function


        public virtual void Accept_Employee_Detail()
        {
            base.AcceptPersonDetail();

            Console.WriteLine("please enter Employee Passcode ");
           this.Passcode = Console.ReadLine();

            Console.WriteLine("please enter Employee Department no  ");
            this.DeptNo = Convert.ToInt32(Console.ReadLine());


            Console.WriteLine("please enter Employee hire date in day month year format");
            int day1 = Convert.ToInt32(Console.ReadLine());
            int month1 = Convert.ToInt32(Console.ReadLine());
            int year1 = Convert.ToInt32(Console.ReadLine());
            this.Hiredate = this.GetBirthDate(day1, month1, year1);




            Console.WriteLine("please enter Employee salary? \n please enter salary as double value \n e.g. 100.50");
            this.salary = Convert.ToDouble(Console.ReadLine());
            this.hra = this._salary * 0.4;
            this.da = this._salary * 0.1;



        }

      

        public virtual string Employee_Detail()
        {
            return " employee id = " + " " + this.id.ToString() +
                " " + " \n employee name = " + " " + this.name +
                " " + " \n employee gendor = " + " " + this.gender.ToString() +
              " " + "employee email address = " + " " + this.EmailId +
              " " + "\n employee address = " + " " + this.address + " " +
              " \n employee birth date = " + " " + this.Date_Detail() + " " +
                 "employee age = " +  this.GetAge(this.birth).ToString() + "\n" +
              " " + " \n employee Hire date = " + " " + this.Hiredate.Date_Detail() + " " +
              "employee salary" + " " + this._salary.ToString() + " " +
                " \n employee  " + this.GetNetSalary() + " "
                + "emp designation = " + " " + this.designation
                + " " + "employee type = " + " " + this.empType.ToString() + "\n"
                + "employee passcode =  " + this.Passcode + "\n" + "employee Department no = " + this.DeptNo.ToString();
                ;
        }


        public virtual string GetNetSalary()
        {
            double netSalary = this.salary + this.hra + this.da;
            return "Net Salary = " + " " + netSalary.ToString();
        }


       /* public Date GetHireDate(int day,int month,int year)
        {
            return new Date(day, month, year);
        }*/
        #endregion



    }
    public class Salesperson:Employee
    {
        #region problem statement
        /*Write a class Salesperson in HRMSLib project with following members:
          double _commision ;
          Provide following functionality:
          Default constructor
          Parameterized constructor
          Properties: Commision
          string ToString(); override method to return data of object in string format.
          Hint: class must be inherited from Employee class so that designation is fixed i.e. “Salesperson”.
          Netsalary = Salary + HRA + DA + Commision
          git commit –m “assignment 4.5 completed”
                   */
        #endregion

        #region data member
        private double _commision;
        #endregion


        #region Constructor
        public Salesperson():base()
        {
            this.commision = 0.0;

            this.designation = "Salesperson";
        }

        public Salesperson(double salary, string designation, EmployeeTypes emptype, string passcode, int deptno, Date hireDate, string Name, bool gender,string email, string add, Date date, double commision)
            : base(salary, designation, emptype,passcode,deptno,hireDate,Name, gender,email, add, date)
        {
            this.commision = commision;
           
        }
        #endregion

        #region getter and setter
        public double commision
        {
            get { return _commision; }
            set { _commision = value; }
        }

        #endregion


        #region member Function

        public override void  Accept_Employee_Detail()
        {
            base.Accept_Employee_Detail();

            Console.WriteLine("do you want to change commision amount ? \n  ente double value \n eg. 100.50");
           this.commision = Convert.ToDouble(Console.ReadLine());

        }



        public override string Employee_Detail()
        {

            return base.Employee_Detail() + "employee commision = " + " " + this.commision.ToString() + " \n";
        }


        public override string GetNetSalary()
        {
            double netSalary = this.salary + this.hra + this.da + this.commision;
            return "Net Salary = " + " " + netSalary.ToString();
        }
        #endregion

    }


    public class WageEmp:Employee
    {

        #region Problem Statement
        /*Write a class WageEmp (Contract basis employee) in HRMSLib project with following members:
          int hours; int rate; (Per hour basis)
          Provide following functionality:
          Default constructor
          Parameterized constructor
          Properties: Hours, Rate
          string ToString(); override method to return data of object in string format.
          Hint: class must be inherited from Employee class so that designation is fixed i.e. “Wage”.
          Netsalary = Hours * Rate
          git commit –m “assignment 4.6 completed”
         */
        #endregion

        #region data member
        private int _hours;

        private int _rate;
        #endregion



        #region Constructor
        public WageEmp() : base()
        {
            this.hour = 0;
            this.rate = 0;
            this.designation = "Wage";



        }

        public WageEmp(double salary, string designation, EmployeeTypes emptype, string passcode, int deptno, Date hiredate, string Name, bool gender,string email, string add, int day, int month, Date date)
            : base(salary, designation, emptype,passcode,deptno,hiredate, Name, gender,email, add, date)
        {
            this.hour = hour;
            this.rate = rate;

        }
        #endregion


        #region getter and setter
        public int rate
        {
            get { return _rate; }
            set { _rate = value; }
        }


        public int hour
        {
            get { return _hours; }
            set { _hours = value; }
        }

        #endregion



        #region member Function


        public override void Accept_Employee_Detail()
        {
            base.Accept_Employee_Detail();

            Console.WriteLine("do you want to change rate per hour amount ? \n  ente int value \n by default set to 20");
            this.rate = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("do you want to change hour worked by wage employee ? \n  ente int value \n by default set to 10");
            this.hour = Convert.ToInt32(Console.ReadLine());


        }





        public override string Employee_Detail()
        {
          

            return base.Employee_Detail() +  " " +
              " employee wage rate per hour = " + " " + this.rate.ToString() +
              " " + " employee work hours = " + " " + this.hour.ToString() + " " + " \n";
        }



        public override string GetNetSalary()
        {
            double netSalary = this.rate * this.hour;
            return "Net Salary = " + " " + netSalary.ToString();
        }
        #endregion


    }

    public class Department1:Employee
    {

        #region data member
        private int _deptNo;
        private string _deptName;
        private string _location;
        #endregion

        #region constructor
        public Department1():base()
        {
            this.deptName = "IT";

            this.deptNo = 1;
            this.location = "mumbai";
        }
 public Department1(double salary, string designation, EmployeeTypes emptype, string passcode, Date hireDate, string Name, bool gender, string email, string add, Date date,int deptno, string deptname, string location)
            :base(salary, designation, emptype, passcode, deptno, hireDate, Name, gender, email, add, date)
        {
            this.deptNo = deptno;
            this.deptName = deptname;
            this.location = location;
        }
        #endregion


        #region getter and setter
        public string location
        {
            get { return _location; }
            set { _location = value; }
        }

        public string deptName
        {
            get { return _deptName; }
            set { _deptName = value; }
        }

        public int deptNo
        {
            get { return _deptNo; }
            set { _deptNo = value; }
        }
        #endregion

        #region member function


        public override void Accept_Employee_Detail()
        {
            base.Accept_Employee_Detail();

            //Console.WriteLine("Enter department no");
            //this.deptNo = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Enter department name ");
            this.deptName = Console.ReadLine();

            Console.WriteLine("Enter department location  ");
            this.location = Console.ReadLine();

        }

        public override string Employee_Detail()
        {

            return base.Employee_Detail() + "department no = " + " " + this.deptNo.ToString() +
                " " + "department name = " + " " + this.deptName +
                " " + " department location = " + " " + this.location + " \n";
        }


        #endregion
    }

    public class Department 
    {

        #region data member
        private int _deptNo;
        private string _deptName;
        private string _location;
        #endregion

        #region constructor
        public Department() : base()
        {
            this.deptName = "IT";

            this.deptNo = 1;
            this.location = "mumbai";
        }
        public Department( int deptno, string deptname, string location)
        {
            this.deptNo = deptno;
            this.deptName = deptname;
            this.location = location;
        }
        #endregion


        #region getter and setter
        public string location
        {
            get { return _location; }
            set { _location = value; }
        }

        public string deptName
        {
            get { return _deptName; }
            set { _deptName = value; }
        }

        public int deptNo
        {
            get { return _deptNo; }
            set { _deptNo = value; }
        }
        #endregion

        #region member function


        public  void Accept_Department_Detail()
        {
           // base.Accept_Employee_Detail();

            Console.WriteLine("Enter department no");
           this.deptNo = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Enter department name ");
            this.deptName = Console.ReadLine();

            Console.WriteLine("Enter department location  ");
            this.location = Console.ReadLine();

        }

        public string Get_Department_Detail()
        {

            return  "department no = " + " " + this.deptNo.ToString() +
                " " + "department name = " + " " + this.deptName +
                " " + " department location = " + " " + this.location + " \n";
        }


        #endregion
    }
}






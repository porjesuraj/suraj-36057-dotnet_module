﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using HRMSLib;
namespace Menu_driven_app
{
    class Program
    {
        static void Main(string[] args)
        {

            #region problem statement
            /*Create a console application and use the library “HRMSLib” as private assembly. Console
              application must be a menu drive program providing facilities of Accepting/Printing Employee
              details
              git commit –m “assignment 4.7 completed”
             */
            #endregion

            #region menu-driven programme
            char ischoice = 'y';
            do
            {
                Console.WriteLine("Please Enter your choice 1.Employee \n  2.SalesPerson \n 3. WageEmployee \n 4. Department-employee \n 5. Department \n ");
                int choice = Convert.ToInt32(Console.ReadLine());

                if (choice == 5)
                {
                    Department dp = new Department();
                    dp.Accept_Department_Detail();
                    Console.WriteLine(dp.Get_Department_Detail());
                  

                }
                else
                {
                    EmployeeFactory empfactory = new EmployeeFactory();

                    Employee emp = empfactory.GenerateEmployeeObject(choice);

                    emp.Accept_Employee_Detail();
                    Console.WriteLine(emp.Employee_Detail());
                }


                Console.WriteLine("do you want to continue to fill and display employee details ? \n press y for yes \n press n for no");
                ischoice = Convert.ToChar(Console.ReadLine());
            } while (ischoice == 'y');

            #endregion


            Console.WriteLine();
        }
    }

    public class EmployeeFactory
    {
        public Employee GenerateEmployeeObject(int choice)
        {
            if(choice == 1)
            {
                return new Employee();
            }
            else if (choice == 2)
            {
                return new Salesperson();
            } else if(choice == 3)
            {
                return new WageEmp();
            } else
            {
                return new Department1();
            }

        }

    }
}

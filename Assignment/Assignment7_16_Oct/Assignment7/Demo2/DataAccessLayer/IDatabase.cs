﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Demo2.PlainOldClrObject;
namespace Demo2.DataAccessLayer
{
     public interface IDatabase
    {
        List<Emp> Select();

        int Insert(Emp emp);

        int Update(Emp emp);

        int Delete(Emp emp);

    }
}

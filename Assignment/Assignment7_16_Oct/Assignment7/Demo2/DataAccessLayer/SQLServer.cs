﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Demo2.PlainOldClrObject;
using Demo2.DataAccessLayer;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace Demo2.DataAccessLayer
{
     public class SQLServer:IDatabase
    {
       public List<Emp> Select()
        {
            string sqlconnect = ConfigurationManager.ConnectionStrings["sqlconstring"].ToString();

            SqlConnection con = new SqlConnection(sqlconnect);
            try
            {
                List<Emp> emplist = new List<Emp>();
                


                string query = "select * from Emp";

                SqlCommand cmd = new SqlCommand(query, con);

                con.Open();

                SqlDataReader reader = cmd.ExecuteReader();


                while (reader.Read())
                {
                    Emp emp = new Emp()
                    {
                        No = Convert.ToInt32(reader["No"]),
                        Name = reader["Name"].ToString(),
                        Address = reader["Address"].ToString()
                    };

                    emplist.Add(emp);
                }

                return emplist;

            }
            catch (Exception ex)
            {

                Console.WriteLine("Error message = " + ex.Message);

                return null;
            }
            finally
            {
                con.Close();
            }


           
           
        }

        public int Insert(Emp emp)
        {
            string sqlconnect = ConfigurationManager.ConnectionStrings["sqlconstring"].ToString();

            SqlConnection con = new SqlConnection(sqlconnect);
            try
            {
               

               // string query = "insert into Emp values({0},'{1}','{2}')";
                //string finalQuery = string.Format(query, emp.No, emp.Name, emp.Address);
                
                SqlCommand cmd = new SqlCommand("spInsert", con);

                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter p1 = new SqlParameter("@No", SqlDbType.Int);
                p1.Value = emp.No;

                SqlParameter p2 = new SqlParameter("@Name", SqlDbType.VarChar, 50);
                p2.Value = emp.Name;

                SqlParameter p3 = new SqlParameter("@Address", SqlDbType.VarChar, 50);

                p3.Value = emp.Address;

                cmd.Parameters.Add(p1);
                cmd.Parameters.Add(p2);
                cmd.Parameters.Add(p3);

                con.Open();

                int rowsAffected = cmd.ExecuteNonQuery();
                Console.WriteLine("Record Inserted using Stored Procedure...");
                return rowsAffected;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Record Insertion Failed using Stored Procedure...");
                Console.WriteLine("Error message = " + ex.Message);

                return 0;
            }
            finally
            {
                con.Close();
            }

          

           

           
            
        }

        public int Update(Emp emp)
        {

            string sqlconnect = ConfigurationManager.ConnectionStrings["sqlconstring"].ToString();

            SqlConnection con = new SqlConnection(sqlconnect);
            try
            {

                
                SqlCommand cmd = new SqlCommand("spUpdate", con);

                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter p1 = new SqlParameter("@No", SqlDbType.Int);
                p1.Value = emp.No;

                SqlParameter p2 = new SqlParameter("@Name", SqlDbType.VarChar, 50);
                p2.Value = emp.Name;

                SqlParameter p3 = new SqlParameter("@Address", SqlDbType.VarChar, 50);

                p3.Value = emp.Address;

                cmd.Parameters.Add(p1);
                cmd.Parameters.Add(p2);
                cmd.Parameters.Add(p3);

                con.Open();



                int rowsAffected = cmd.ExecuteNonQuery();
                Console.WriteLine("Record Updated using Stored Procedure...");
                return rowsAffected;
            }
            catch (Exception ex)
            {

                Console.WriteLine("Record Updation Failed using Stored Procedure...");
                Console.WriteLine("Error message = " + ex.Message);

                return 0;
            }
            finally
            {
                con.Close();
            }
        }



        public int Delete(Emp emp)
        {
            #region problem statement 
            /*Don’t create stored procedure for delete operation use parameterized query
               use
            string query = "delete from emp where No=@No";
             cmd.CommandType = CommandType.Text
 //create SQL parameter object and attach parameter to command
 git commit –m “Assignment no. 7.4 completed”*/
            #endregion



            string sqlconnect = ConfigurationManager.ConnectionStrings["sqlconstring"].ToString();

            SqlConnection con = new SqlConnection(sqlconnect);
            try
            {
               SqlParameter p1 = new SqlParameter("@No", SqlDbType.Int);
                p1.Value = emp.No;


                string query = "delete from Emp where  No = @No";


                SqlCommand cmd = new SqlCommand(query, con);

                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add(p1);

                con.Open();
                int rowsAffected = cmd.ExecuteNonQuery();

                return rowsAffected;
            }
            catch (Exception ex)
            {

                Console.WriteLine("Error message = " + ex.Message);

                return 0;
            }
            finally
            {
                con.Close();
            }
        }
    }
}

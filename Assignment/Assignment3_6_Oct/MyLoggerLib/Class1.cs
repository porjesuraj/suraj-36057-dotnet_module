﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyLoggerLib
{

    #region problem statement

    /*Open MyLogger.cs file and do following changes in MyLogger class:
      a. Delete method Log method which was already written
      b. MyLogger class implements ILogger interface i.e. method Log (string message) to log
      messages in text file (csv format) refer figure 1 and this method should meet following
      requirements:
      i. For everyday new log file should be created e.g. Log09102020.csv, Log10102020.csv
      Hint: string filename = "Log" + DateTime.Now.ToString("ddMMyyyy") + ".csv";
      ii. Store log details into separate text file in append mode. Also print log details on
      console.
      Figure
     */
    #endregion

    public interface ILogger
    {
        void Log(string message);
    }
    public class Logger:ILogger
    {
        private static Logger _logger = new Logger();
        private Logger()
        {
            Console.WriteLine("Logger Object Created ...");

        }

        public static Logger CurrentLogger
        {
            get { return _logger; }
        }
           public void Log(string message)
        {
            string filename = "Log" + DateTime.Now.ToString("ddMMyyyy") + ".csv";
            #region create log file
            

            FileStream fs = new FileStream(@"E:\DAC\.NET_module\Assignment\Assignment3_6_Oct\MyLoggerLib\" + filename,
                                           FileMode.OpenOrCreate, FileAccess.Write);

            

            fs.Flush();
            fs.Close();
            #endregion

            #region write to file
            FileStream fs2 = new FileStream(@"E:\DAC\.NET_module\Assignment\Assignment3_6_Oct\MyLoggerLib\" + filename,
                                         FileMode.Append, FileAccess.Write);

            StreamWriter writer = new StreamWriter(fs2);

            writer.WriteLine( DateTime.Now.ToString() + " ," + message);

            fs2.Flush();

            writer.Close();

            fs2.Close();
            #endregion

            #region read from file 
            FileStream fs3 = new FileStream(@"E:\DAC\.NET_module\Assignment\Assignment3_6_Oct\MyLoggerLib\" + filename,
                                         FileMode.Open, FileAccess.Read);

            StreamReader reader = new StreamReader(fs3);

                string readingFile =   reader.ReadToEnd();

            Console.WriteLine(readingFile);

            
             
           
            reader.Close();
            fs3.Close();
            #endregion

            


        }



    }







    #region logger class assignment 3
    /*public class Logger
    {
        private static Logger _logger = new Logger();
        private Logger()
        {
            Console.WriteLine("Logger Object Created ...");

        }

        public static Logger CurrentLogger
        {
            get { return _logger; }
        }

        public void Log(string msg)
        {
            Console.WriteLine(" logger message : " 
                              + msg 
                              + " "
                              + "@"
                              + DateTime.Now.ToString());

        }
    }*/
    #endregion

}

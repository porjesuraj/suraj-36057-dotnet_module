﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace test
{
    class Program
    {
        static void Main(string[] args)
        {
            string filename = "Log" + DateTime.Now.ToString("ddMMyyyy") + ".txt";
          

           // FileStream fs = new FileStream(@"E:\DAC\.NET_module\Assignment\Assignment3_6_Oct\MyLoggerLib\" + filename,
                                 //          FileMode.OpenOrCreate, FileAccess.Write);

            #region Normal File Writing
           // FileStream fs = new FileStream("E:\\DAC\\claswork-bofore-git-commit\\Day7\\Data.txt")
             // declare stream with path,mode,access
             FileStream fs = new FileStream(@"E:\DAC\.NET_module\Assignment\Assignment3_6_Oct\MyLoggerLib\" + filename,
                                            FileMode.OpenOrCreate,
                                            FileAccess.Write);
             // need writer to write to file , for stream object fs 

             StreamWriter writer = new StreamWriter(fs);
             //use writer
             writer.WriteLine("hello world ");

             fs.Flush();  // forcefully write on harddrive if data is still in Ram not written due to small data size
             writer.Close();
             fs.Close(); // content in memory //ram still need to be written to file for small data written so use close 
 
            #endregion


            Console.ReadLine();
        }
    }
}
﻿/*Create Console Application
Write a structure Book with following members:
	string title; bool outofstock; string author;
int isbn; char index; double price;
Provide following functionality:
	Default constructor(?)
	Parameterized constructor
	Get and Set methods
	AcceptDetails() method to accept data from console.
	PrintDetails() method to print data to console.

Check the default access specifier for members of structure.
Keep all data members private and all member functions public.
Upload solution on git
*/


using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace assignment2_Q1
{
    class Program
    {

        struct Book
        {
          
            private string title;
            private bool outofstock;
            private string author;
            private int isbn;
            private char index;
            private double price;

            public Book (string title,bool outofstock,string author,int isbn,char index,double price)
            {
                this.title = title;
                this.outofstock = outofstock;
                this.author = author;
                this.isbn = isbn;
                this.index = index;
                this.price = price;

            }
            //Structure does not contain parameter less constructor or destructor, but can contain Parameterized constructor or static constructor

            //getter function

            public string getTitle()
            {
                return this.title;
            }

            public bool getOutOfStock()
            {
                return this.outofstock;
            }
            public string getAuthor()
            {
                return this.author;
            }
            public int getIsbn()
            {
                return this.isbn;
            }
            public char getIndex()
            {
                return this.index;
            }

            public double getPrice()
            {
                return this.price;
            }

            //setter function 
            public void setTitle(string title)
            {
                this.title = title;
            }
            public void setOutOfStock(bool outofstock)
            {
                this.outofstock = outofstock;
            }

            public void setAuthor(string author)
            {
                this.author = author;
            }
            public void setIsbn(int isbn)
            {
                this.isbn = isbn;
            }
            public void setIndex(char index)
            {
                this.index = index;
                    
                     
            }

            public void setPrice(double price)
            {
                this.price = price;
            }


            // AcceptDetails() method to accept data from console.
            // string title,bool outofstock,string author,int isbn,char index,double price
            public void AcceptDetails()
            {
                Console.WriteLine("Please enter title of book as a string value ");
               // this.title = Convert.ToString(Console.ReadLine());
                setTitle(Convert.ToString(Console.ReadLine()));
                Console.WriteLine("Please enter OutofStock value of book as a boolean value");
                this.outofstock = Convert.ToBoolean(Console.ReadLine());

                Console.WriteLine("Please enter author  of book  as a string value");
                this.author = Convert.ToString(Console.ReadLine());

                Console.WriteLine("Please enter isbn  of book as int value");
                this.isbn = Convert.ToInt32(Console.ReadLine());

                Console.WriteLine("Please enter index of book as char value");
                this.index = Convert.ToChar(Console.ReadLine());

                Console.WriteLine("Please enter price of book as double value");
                this.price = Convert.ToDouble(Console.ReadLine());


            }

            //PrintDetails() method to print data to console.
            //// string title,bool outofstock,string author,int isbn,char index,double price
            ///

            public void PrintDetails()
            {
                Console.WriteLine(" book details \n ");
                Console.WriteLine(" \n 1. book title = "+ getTitle() +
                    "\n 2. book outofstock value = " +  getOutOfStock() + 
                    "\n 3. book author name =  " + getAuthor() +
                    "\n 4. book  International Standard Book Number = " + getIsbn() + 
                    "\n 5. book index = " + getIndex() + 
                    "\n 6. book price =  " + getPrice() );
            }

            public void Execute()
            {
                AcceptDetails();
                PrintDetails();
                Console.ReadLine();
            }
        }


        static void Main(string[] args)
        {
            // parameterless 
            Book b1 = new Book();

            b1.setTitle("tuesday with moorie");
            b1.setOutOfStock(false);
            b1.setAuthor("Mitch Albom");
            b1.setIsbn(001);
            b1.setIndex('A');
            b1.setPrice(100);

             b1.PrintDetails();
             b1.AcceptDetails();
             b1.PrintDetails();

            // parameterize
            //string title,bool outofstock,string author,int isbn,char index,double price

            Book b2 = new Book("alchemist",false,"paul coelho",003,'A',100.00);
            b2.PrintDetails();

            Console.ReadLine();

            // use execute method
            b2.Execute();

        }
    }
}

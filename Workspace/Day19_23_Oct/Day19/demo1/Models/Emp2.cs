﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using demo1.Logger;

namespace demo1.Models
{
    [MetadataType(typeof(ExtraInfo_About_Emp))]
    public partial class Emp
    {

    }


    public class ExtraInfo_About_Emp
    {
        [Required(ErrorMessage = "No is must")]
        [Range(1, 100, ErrorMessage = "No must be between 1 to 100")]
        public int No { get; set; }

        [Required(ErrorMessage = "Name is must")]
        [DemoValidator(ErrorMessage = "1234 is not a valid Name")]
        public string Name { get; set; }


        [Required(ErrorMessage = "Address is must")]
        public string Address { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using demo1.Logger;
namespace demo1.Filters
{
    #region  action and result filter interface added to class
    /*public class CustomFilter : IActionFilter, IResultFilter
    {
        public void OnActionExecuted(ActionExecutedContext filterContext)
        {
            throw new NotImplementedException();
        }

        public void OnActionExecuting(ActionExecutingContext filterContext)
        {
            throw new NotImplementedException();
        }

        public void OnResultExecuted(ResultExecutedContext filterContext)
        {
            throw new NotImplementedException();
        }

        public void OnResultExecuting(ResultExecutingContext filterContext)
        {
            throw new NotImplementedException();
        }
    }*/


    #endregion


    public class CustomFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            Logger.Logger.CurrentLogger.Log(string.Format("{0}/{1} is about to execute",
                
                filterContext.ActionDescriptor.ControllerDescriptor.ControllerName,
                filterContext.ActionDescriptor.ActionName));
        }

        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            Logger.Logger.CurrentLogger.Log(string.Format("{0}/{1}  executed",

                filterContext.ActionDescriptor.ControllerDescriptor.ControllerName,
                filterContext.ActionDescriptor.ActionName));
        }

        public override void OnResultExecuting(ResultExecutingContext filterContext)
        {
            Logger.Logger.CurrentLogger.Log(string.Format("UI Processing is about to begin"));
        }

        public override void OnResultExecuted(ResultExecutedContext filterContext)
        {
            Logger.Logger.CurrentLogger.Log(string.Format("UI Processing is done"));
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace demo2DB
{
    class Program
    {
        static void Main(string[] args)
        {

            

            try
            {
                char ischoice = 'y';
                do
                {
                    Console.WriteLine(" Sql operation  to perform on Emp?" +
                    "  1. for Select \n 2. Insert \n 3.Update \n 4. Delete \n");

                    int choice = Convert.ToInt32(Console.ReadLine());
                    DbOperations operation = new DbOperations();

                    switch (choice)

                    {
                        case 1:
                            List<string> data = operation.SelectOperation();

                            foreach (string d in data)
                            {
                                Console.WriteLine(d);
                            }

                            break;
                        case 2:
                            operation.InsertOperation();

                            break;
                        case 3:
                            operation.UpdateOperation();
                            break;
                        case 4:
                            operation.DeleteOperation();

                            break;

                        default:
                            break;
                    }

                    Console.WriteLine("do you want to continue? if press y or n");
                    ischoice = Convert.ToChar(Console.ReadLine());

                } while (ischoice == 'y');


            }
            catch (Exception ex )
            {

                Console.WriteLine(string.Format("error message = {0}", ex.Message));
            }


           
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace demo1
{
    class Program
    {
        static void Main(string[] args)
        {
            #region partial method
            /* Emp emp = new Emp();

             emp.Age = 9;*/
            #endregion

            #region dynamic type
            /* Factory factory = new Factory();

             Console.WriteLine("Enter choice as  number");
             int choice = Convert.ToInt32(Console.ReadLine());

             dynamic obj = factory.GetSomeTypeObject(choice);

             Console.WriteLine(obj.GetDetails());
 */
            /*if(obj is Emp)
            {
                Emp e = new Emp();
                Console.WriteLine(e.GetDetails());

            } else if (obj is Book)
            {
                Book b = new Book();
                Console.WriteLine(b.GetBookDetails());

            }*/
            #endregion


            #region Optional /named parameter
            Player player = new Player();

            string msg = player.GetDetails(1, "suraj", "mumbai");
            Console.WriteLine(msg);

            string msg2 = player.GetDetails(1);
            Console.WriteLine(msg2);

            string msg3 = player.GetDetails(1, Name: "suraj");
            Console.WriteLine(msg3);

            string msg4 = player.GetDetails(1, Address: "nasik");
            Console.WriteLine(msg4);
            //no need to overload parameters ,we can use default/named parameter 
            #endregion

            Console.ReadLine();
        }
       
    }

    public class Player
    {
        public string GetDetails(int No,string Name="ABCD",string Address="mumbai")
        {
            return string.Format("player no = {0}, Name = {1}, Address = {2} ", No, Name, Address);
        }
    }


}

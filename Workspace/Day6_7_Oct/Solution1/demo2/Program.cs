﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace demo2_case_study
{
    class Program
    {
        static void Main(string[] args)
        {

            #region Using Person Colletion To Hold Data of Emp & Customer
            /*
              Employee emp1 = new Employee();
            emp1.Name = "SURAJ";
            emp1.No = 1;
            emp1.DeptName = "IT";

            Employee emp2 = new Employee();
            emp2.Name = "RAJ";
            emp2.No = 2;
            emp2.DeptName = "IT";
            Employee emp3 = new Employee();
            emp3.Name = "RAJU";
            emp3.No = 3;
            emp3.DeptName = "IT";

            Customer c1 = new Customer();
            c1.Name = "sagar";
            c1.No = 10;
            c1.OrderDetails = "Bicycle";

            Customer c2 = new Customer();
            c2.Name = "saraa";
            c2.No = 11;
            c2.OrderDetails = "Gym equipments";

            Person[] arr = new Person[5];

            arr[0] = emp1;
            arr[1] = emp2;
            arr[2] = emp3;
            arr[3] = c1;
            arr[4] = c2;


            for (int i = 0; i < arr.Length; i++)
            {
                if(arr[i] != null)
                { 
                Person p = arr[i];
                Console.WriteLine(p.Details());
                    }
            }
             */
            #endregion


            #region Using Person collection, typecasting employeea and customer
            Employee emp1 = new Employee();
            emp1.Name = "SURAJ";
            emp1.No = 1;
            emp1.DeptName = "IT";

            Employee emp2 = new Employee();
            emp2.Name = "RAJ";
            emp2.No = 2;
            emp2.DeptName = "IT";
            Employee emp3 = new Employee();
            emp3.Name = "RAJU";
            emp3.No = 3;
            emp3.DeptName = "IT";

            Customer c1 = new Customer();
            c1.Name = "sagar";
            c1.No = 10;
            c1.OrderDetails = "Bicycle";

            Customer c2 = new Customer();
            c2.Name = "saraa";
            c2.No = 11;
            c2.OrderDetails = "Gym equipments";

            Person[] arr = new Person[5];

            arr[0] = emp1;
            arr[1] = emp2;
            arr[2] = emp3;
            arr[3] = c1;
            arr[4] = c2;


            for (int i = 0; i < arr.Length; i++)
            {
                if (arr[i] != null)
                {
                    if (arr[i] is Employee)
                    {
                       // Employee e = (Employee)arr[i];
                        //Console.WriteLine(e.Details());
                        Console.WriteLine(((Employee)arr[i]).Details());
                    }
                    else if (arr[i] is Customer)
                    {
                        Customer c = (Customer)arr[i];
                        Console.WriteLine(c.Details());
                    }

                }
            }
            #endregion




            Console.ReadLine();
        }
    }

    public class Person
    {


        private int _No;

        private string _Name;


        //public Person(string name,int no)
        //{
        //    this.Name = name;
        //    this.No = no;
        //}

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }
        public int No
        {
            get { return _No; }
            set { _No = value; }
        }

        public virtual string Details()
        {
            return "Name = " + this.Name.ToString() + " " + "No = " + this.No.ToString();
        }

    }


    public class Employee : Person
    {
        private string _DeptName;


        //public Employee(string name,int no,string deptname) : base(name,no)
        //{
        //    this.DeptName = deptname;
        //}




        public string DeptName
        {
            get { return _DeptName; }
            set { _DeptName = value; }
        }
        public override string Details()
        {
            return base.Details() + " " + "deptarment name = " + this.DeptName;
        }

    }

    public class Customer : Person
    {

        private string _OrderDetails;

        // public Customer(string name, int no, string order) : base(name, no)
        // {
        //     this.OrderDetails = order;
        // }
        public string OrderDetails
        {
            get { return _OrderDetails; }
            set { _OrderDetails = value; }
        }


        public override string Details()
        {
            return base.Details() + " " + "Order Details = " + this.OrderDetails;
        }

    }

}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace demo3_menu_driven
{
    class Program
    {
        static void Main(string[] args)
        {
            ArrayList arr = new ArrayList();

            char ischoice = 'y';
            do
            {
                Console.WriteLine(" Enter enter choice whose value you want to enter 1.Employee 2.Book");
                int choice = Convert.ToInt32(Console.ReadLine());



                switch (choice)
                {
                    case 1:
                        Employee emp = new Employee();

                        Console.WriteLine("enter employee no ");

                        emp.No = Convert.ToInt32(Console.ReadLine());


                        Console.WriteLine("enter employee name ");

                        emp.Name = Console.ReadLine();


                        arr.Add(emp);
                        break;
                    case 2:
                        Book book = new Book();

                        Console.WriteLine("enter book title");
                        book.Title = Console.ReadLine();

                        Console.WriteLine("enter book Author");
                        book.Author = Console.ReadLine();

                        arr.Add(book);
                        break;

                    default:
                        break;
                }


                Console.WriteLine(" SO YOU STILL WANT TO CONTINUE ? \n y/n");
                ischoice = Convert.ToChar(Console.ReadLine());

            } while (ischoice == 'y');

            if (ischoice != 'y')
            {
                for (int i = 0; i < arr.Count; i++)
                {
                    if (arr[i] is Employee)
                    {
                        Console.WriteLine(((Employee)arr[i]).GetEmpDetails());
                    }
                    else if (arr[i] is Book)
                    {
                        Book b = (Book)arr[i];
                        // Console.WriteLine(((Book)arr[i]).GetBookDetails());
                        Console.WriteLine(b.GetBookDetails());
                    }

                }
            }
            Console.ReadLine();





















        }
    }

    public class Employee
    {

        private int _No;

        private string _Name;

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }


        public int No
        {
            get { return _No; }
            set { _No = value; }
        }

        public string GetEmpDetails()
        {
            return "employee no =  " + this.No.ToString() + " " + "employee name = " + this.Name;
        }


    }

    public class Book
    {
        private string _Title;

        private string _Author;

        public string Author
        {
            get { return _Author; }
            set { _Author = value; }
        }


        public string Title
        {
            get { return _Title; }
            set { _Title = value; }
        }


        public string GetBookDetails()
        {
            return "book title = " + this.Title + " " + "book author = " + this.Author;
        }
    }
}

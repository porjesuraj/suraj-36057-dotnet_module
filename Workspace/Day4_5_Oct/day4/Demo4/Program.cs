﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyLoggerLib;
namespace Demo4
{
    class Program
    {
        static void Main(string[] args)
        {

            char ischoice = 'y';

            do
            {
                Console.WriteLine("Please select database \n 1.SQL \n 2.Oracle \n");

                int choice = Convert.ToInt32(Console.ReadLine());

                ObjectFactory objectfactory = new ObjectFactory();
                Database database = objectfactory.GenerateDatabase(choice);

                Console.WriteLine("please enter operation to perform : \n 1. Create 2. Insert \n 3. Update \n 4. Delete \n");
                int operationChoice = Convert.ToInt32(Console.ReadLine());

                switch (operationChoice)
                {
                    case 1:
                        database.Create();
                        break;
                    case 2:
                        database.Insert();
                        break;
                    case 3:
                        database.Update();
                        break;
                    case 4:
                        database.Delete();
                        break;

                    default:
                        break;
                }

                Console.WriteLine("do you want to continue ? if yes press y if no ,press n");
                ischoice = Convert.ToChar(Console.ReadLine());
            } while (ischoice == 'y');



            if (ischoice != 'y')
            {
                Logger.CurrentLogger.Log("user logged off");
             
            }


            Console.ReadLine();
        }
    }

    public class ObjectFactory
    {
        public Database GenerateDatabase(int choice)
        {
            if(choice == 1)
            {
                Logger.CurrentLogger.Log("Sql database object created");
                return new SQLDatabase();
              
            }
            else
            {
                Logger.CurrentLogger.Log("Oracle database object created");
                return new OracleDatabase();
            }

        }
    }


    abstract public class Database
    {
      abstract  public void Create();
       abstract public void Insert();
      abstract  public void Update();
       abstract public void Delete();
    }

    public class SQLDatabase:Database
    {
        public override void Create()
        {
            //Console.WriteLine("create operation done in sql");

            Logger.CurrentLogger.Log("create operation done in sql");
        }
        public override void Insert()
        { 
          //  Console.WriteLine("Insert operation done in sql");
            Logger.CurrentLogger.Log("Insert operation done in sql");
        }
        public override void Update()
        {
           // Console.WriteLine("Update operation done in sql");
            Logger.CurrentLogger.Log("update operation done in sql");
        }
        public override void Delete()
        {
          //  Console.WriteLine("Delete operation done in sql");
            Logger.CurrentLogger.Log("delete operation done in sql");
        }

    }

    public class OracleDatabase : Database
    {
        public override void Create()
        {
           // Console.WriteLine("create operation done in Oracle");
            Logger.CurrentLogger.Log("create operation done in oracle");
        }
        public override void Insert()
        {
           // Console.WriteLine("Insert operation done in Oracle");
            Logger.CurrentLogger.Log("Insert operation done in oracle");
        }
        public override void Update()
        {
           // Console.WriteLine("Update operation done in Oracle");
            Logger.CurrentLogger.Log("Update operation done in oracle");
        }
        public override void Delete()
        {
           // Console.WriteLine("Delete operation done in Oracle");
            Logger.CurrentLogger.Log("Delete operation done in oracle");
        }

    }

}

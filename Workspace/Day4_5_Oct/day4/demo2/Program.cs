﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace demo2
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("please select extension of report \n 1.PDF \n 2.Excel \n 3.Word \n");

            int choice = Convert.ToInt32(Console.ReadLine());

            switch (choice)
            {
                case 1:
                    PdfReport obj = new PdfReport();
                    obj.Parse();
                    obj.Validation();
                    obj.Save();
                   
                break;

                case 2:
                    ExcelReport obj2 = new ExcelReport();
                    obj2.Parse();
                    obj2.Validation();
                    obj2.Save();

                    break;
                case 3:
                    WordReport obj3 = new WordReport();
                    obj3.Parse();
                    obj3.Validation();
                    obj3.Save();

                    break;


                default:
                    break;
            }
            
            Console.ReadLine();

        }
    }
    
    public class PdfReport
    {
        public void Parse()
        {
            Console.WriteLine("Parsing done for PDF");
        }
        public void Validation()
        {
            Console.WriteLine("Validation done for PDF");
        }
        public void Save()
        {
            Console.WriteLine("Save done for PDF");
        }


    }

    public class ExcelReport
    {
        public void Parse()
        {
            Console.WriteLine("Parsing done for Excel");
        }
        public void Validation()
        {
            Console.WriteLine("Validation done for Excel");
        }
        public void Save()
        {
            Console.WriteLine("Save done for Excel");
        }


    }

    public class WordReport
    {
        public void Parse()
        {
            Console.WriteLine("Parsing done for Word");
        }
        public void Validation()
        {
            Console.WriteLine("Validation done for Word");
        }
        public void Save()
        {
            Console.WriteLine("Save done for Word");
        }


    }
}

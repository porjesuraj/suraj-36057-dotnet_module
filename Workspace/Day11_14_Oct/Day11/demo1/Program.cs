﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace demo1

{
    class Program
    {
        delegate string mydelegate(string name); // in namespace 
        static void Main(string[] args)
        {

            #region nullable
            /*  int? Salary = 0;
              Salary = null;

              int? Salary1 = 110;
              //both are same 
              // Nullable<int> Salary1 = null;

              if (Salary1.HasValue)
              {
                  Console.WriteLine("Salary holds a value");
              }
              else
              {
                  Console.WriteLine("Salary is null");
              }*/

            #endregion

            #region example
            /*
                       Emp e = new Emp();
                       e.Name = "string";
                       e.can = true;
                       if(e.can)
                       {

                       }*/
            #endregion


            #region Partial class usage
            /*  Maths obj = new Maths();

              obj.Add(10, 20);
              obj.Sub(20, 10);*/

            #endregion


            #region Anonymous Method

            // calll normal method 
            /*string message = SayHello("suraj");
            Console.WriteLine(message);

            // SayHello("suraj");
            // call method using  pointer
             mydelegate pointer1 = new mydelegate(SayHello);
             string message2 = pointer1("suraj porje");
              Console.WriteLine(message2);

            //anonymous method
            mydelegate pointer2 = delegate (string name)
            {
                return "Hello" + name;
            };


            string message3 = pointer2("suraj porje");
            Console.WriteLine(message3);*/


            #endregion

            #region Lambada expression
            /* 
          mydelegate pointer1 = new mydelegate(SayHello);
          string message2 = pointer1("suraj porje");
          Console.WriteLine(message2);
          */

            //Lambda expression Syntax
            // same as above code in comment
            /* mydelegate pointer2 = (name) => {
                 return "Hello" + name;
             };


             string message3 = pointer2("suraj porje");
             Console.WriteLine(message3);
             */
            #endregion

            #region Iterator concept implementation
            /*Week week = new Week();

            foreach (string day in week)
            {
                Console.WriteLine(day);
            }*/
            #endregion


            #region Implicit Type
            /*//normal declaration
            int i = 100;
            object obj = i;
            Emp e1 = new Emp();

            obj = e1;
            //implicit declaration
            var v = 100;
            // v = "a"; // gives error

            var v2 = new Emp();

            // var v3; // gives error implicit must be initialized


            int choice = Convert.ToInt32(Console.ReadLine());
            var v5 = GetSomething(choice);
            //var v6 = null; //cannot assign null to implicit type */

            #endregion

            #region Auto Property
            /* Emp emp = new Emp();

             emp.No = 1;
             emp.Name = "suraj";

             Console.WriteLine(emp.Name);*/
            #endregion

            #region Object Initializer 
            //shorthand for assigning values
            /* var emp = new Emp() { No = 100, Name = "suraj", Address = "nasik" };
             var emp2 = new Emp()
             {
                 No = 100,
                 Name = "suraj",
                 Address = "nasik"
             };

             Console.WriteLine(emp.Name);*/
            #endregion


            #region Anonymous Type 
            /*  // commpiler creates a class, which we cannot use other place
              // we can see in MSIL 
              // getter are defined, setter are not,it is not allowed -- i.e it is read only 
              // so datatype of object of anonymous class must be var, as we have no knowledge about type of data member
              // so var at compile time get datatype of object returned, so we need to use var, as we dont know what it on RHS
              var emp = new { No = 100, Name = "suraj", Address = "nasik" };
              // object emp = new { No = 100, Name = "suraj", Address = "nasik" };
              var emp1 = new { No = 100, Name = "suraj", Address = 1 };

              // both class object have one generic class ,with generic data member

              //now it creates a new class in MSIL for this 
              // as parameter sequence matter, values doesnt
              var emp2 = new { No = 100, Address = 1, Name = "suraj" };
              Console.WriteLine(emp.No);
              Console.WriteLine(emp.Name);
              Console.WriteLine(emp.Address);*/
            #endregion


            #region Extension method

             Console.WriteLine("Enter some data");
             string data = Console.ReadLine();

           //  MyUtilityClass obj10 = new MyUtilityClass();

             // obj10.CheckForValidEmailAddress();
  
              // bool result = MyUtilityClass.CheckForValidEmailAddress(data,100);

             //  public static bool CheckForValidEmailAddress(this string s,int i)
             // this makes the method a extension of string 

             bool result = data.CheckForValidEmailAddress(100);  // extension method
             Console.WriteLine(result);
             //Sealed */

             int[] arr = new int[] { 10, 20, 30, 40, 50, 60, 70, 80, 90 };

             Console.WriteLine(arr.Average());

             Console.WriteLine("Enter some data");
             string data2 = Console.ReadLine();


             var v = new { No = 1, Name = "suraj", Address = "nasik" };

             //v.CheckForValidEmailAddress(100);


           //  Console.WriteLine(v.CheckForValidEmailAddress(100));


            #endregion






            Console.ReadLine();
        }

        #region Sealed class
        /*//String is sealed class, as part of product package ,so cant inherit from class
        
        public class MyString:String
        {

        }
        */

        #endregion



        //  var v4 = 100; //var is always a local variable 

        public static object GetSomething(int i)
        {
            if (i == 1)
            {
                return 100;
            }
            else
            {
                //return new Emp();
                return 200;
            }
        }


        public static string SayHello(string name)
        {
            return "Hello" + name;
        }

        public static void Do1<P, Q, R, S>(P p, Q q, R r, S s)
        {

        }
        public static void Do2<P, Q, S, R>(P p, Q q, S s, R r)
        {

        }
    }





    #region iterator concept
    /*public class Week : IEnumerable
    {
        private string[] days =
            new string[] { "mon", "tue", "wed", "thur", "fri", "sat", "sun" };

        public IEnumerator GetEnumerator()
        {
            for (int i = 0; i < days.Length; i++)
            {
                yield return days[i];


            }


            // throw new NotImplementedException();
        }

    }*/
    #endregion




    #region declare Extended method in static class
   public static class MyUtilityClass
    {
        public static bool CheckForValidEmailAddress(this string s, int i)
        {



            return s.Contains("@");

        }
    }

    // generic method 
     public static class MyUtilityClass1
    {
        public static bool CheckForValidEmailAddress<T>(this T s, int i)
        {



            //return s.Contains("@");
            return true;
        }
    }
    
    
    
    #endregion



     public class Emp
     {
        // Auto Property 
         // use prop
         public int No { get; set; }
         public string Name { get; set; }

         public string Address { get; set; }

     }
 

    /* public class Emp
     {
         private string _Name;

         private bool _can;

         private int value;

         public int _value
         {
             get { return value; }
             set { value = value; }
         }


         public bool can
         {
             get { return _can; }
             set { _can = value; }
         }

         public string Name
         {
             get { return _Name; }
             set { _Name = value; }
         }

     }
 */

    public partial class Maths : Somebase1
    {
        public int Add(int x, int y)
        {
            return x + y;
        }
    }

    public class Somebase1
    {

    }

    #region Sealed  used to forbid inheritence of class
    /*public sealed class SomeClass
   {

   }

   public class SomeOtherClass:SomeClass
   {

   }*/
    #endregion



    #region Sealed used to stop override of methods
     /*public sealed class A
     {
         public virtual void M1()
         {

         }
     }

     public class B:A
     {
         public sealed void M1()
         {

         }
     }
     public class C : B
     {
         public void M1()
         {

         }
     }*/
    #endregion

}

﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace demo2

{
    // public delegate bool MyDelegate(int i);
    //using generic delegate 
    public delegate Q MyDelegate<Q, T>(T i);
    class Program
    {
        static void Main(string[] args)
        {
            // MyDelegate<bool,int> pointer = new MyDelegate<bool,int>(Check);

            #region using generic delegate
            /* MyDelegate<bool, int> pointer = delegate (int i) { return (i > 10); };*/
            #endregion


            #region Inbuilt delegaate: Action and Func

            //Action ptr = new Action();
            //use Action when return type is void 
            //Action ptr = new Action();
            // used to point to a method wich return void , and can take 16 parameter

            // Func delegate 
            // used to point to a method wich can return something all the time , and take 16 parameter

            // Func<int, bool> pointer = delegate (int i) { return (i > 10); };
            //   Func<int, bool> pointer = ( i) => { return (i > 10); };

            #endregion






            #region Expression Tree
            // expression tree
            //   Func<int, bool> pointer = ( i) => { return (i > 10); };
            //stage 1: Create a  Expression tree


            Expression<Func<int, bool>> tree = (i) => (i > 10);

            //stage 2: Compile Expression tree
            //compile method keep it in ram 
            Func<int, bool> pointer = tree.Compile();

            // compileToMethod stores method as optimized binary tree in hard drive
            // Func<int, bool> pointer = tree.CompileToMethod();




            Stopwatch watch = new Stopwatch();
            watch.Start();


            //stage 3: in case of expression tree i.e Execute the tree

            //  bool result = pointer(100);

            bool result = pointer(100);

            watch.Stop();

            Console.WriteLine("Time Taken = " + watch.ElapsedTicks.ToString());
            Console.WriteLine("Result is " + result);
            #endregion



            Console.ReadLine();
        }

        public static bool Check(int i)
        {
            return (i > 10);
        }
    }


}




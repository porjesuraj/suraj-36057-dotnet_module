﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace demo3
{
    class Program
    {
        static void Main(string[] args)
        {
            Maths m1 = new Maths();
            Console.WriteLine(m1.Add(10, 20));

            

            Console.WriteLine(m1.Add(10, 20, 30));

            Console.ReadLine();
        }
    }

    public class Maths
    {
        public int Add(int x,int y)
        {
            return x + y;
        }

        public int Add(int x, int y,int z)
        {
            return x + y + z;
        }
    }
}

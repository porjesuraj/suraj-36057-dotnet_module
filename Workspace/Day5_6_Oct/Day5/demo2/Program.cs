﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace demo2
{
    class Program
    {
        static void Main(string[] args)
        {

            #region Below is the way to write Getter Setter like C++
            Person p1 = new Person();
            p1.Name = "suraj";
            p1.Age = 26;

           string person1 =  p1.Detail();
            Console.WriteLine(person1);

            Person p2 = new Person("sagar", 23);

            string person2 =  p2.Detail();

            Console.WriteLine(person2);


            Employee emp = new Employee();
            emp.Name = "suraj";
            emp.Age = 26;
            emp.DName = "IT";

            string employee1 = emp.Detail();
            Console.WriteLine(employee1);

            ConsultantEmployee cemp = new ConsultantEmployee();

            cemp.Name = "aakash";
            cemp.Age = 20;
            cemp.DName = "Gaming";
            cemp.WorkingHours = 30;

            string consultant = cemp.Detail();
            Console.WriteLine(consultant);


            Console.ReadLine();
            #endregion




        }
    }

    public class Person
    {
        #region Data members

        private string _Name;
        private int _Age;
        #endregion


        #region constructor
        public Person()
        {
            this.Name = "";
            this.Age = 0;
        }

        public Person(string name, int age)
        {
            this.Name = name;
            this.Age = age;
        }

        #endregion

        #region Getter and Setter

        /// <summary>
        /// This function helps you set up Name of the person.
        /// you can try setting the name using personObject.Set_name("suraj");
        /// </summary>
        /// <param name="Name">this is nameof a person</param>




        public int Age
        {
            get { return _Age; }
            set { _Age = value; }
        }

       

        public string Name
        {
            get { return "Mr" + " " + _Name; }

            set
            {
                if(value != "")
                {
                    this._Name = value;
                }
                else
                {

                    this._Name = "No Data";
                }
                
               
            }
        }





        #endregion

        #region member function

        public virtual string Detail()
        {
            return  "Person Name = " + this.Name + " " + "Person Age = " + this.Age.ToString();
        }

        #endregion
    }
    public class Employee:Person
    {

        private string _DName;  

        public string DName
        {
            get { return _DName; }
            set { _DName = value; }
        }

        public override string Detail()
        {
            return base.Detail() + " " + "Department name " + DName  ;
        }


    }

    public class ConsultantEmployee:Employee
    {
        private int _WorkingHours;

        public int WorkingHours
        {
            get { return _WorkingHours; }
            set { _WorkingHours = value; }
        }

        public override string Detail()
        {
            return base.Detail() + " " + "Working hours of consultant" + WorkingHours;
        }


    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace Consumer2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("please ente rpath of dll/exe file");
            string path = Console.ReadLine();

            Assembly assembly = Assembly.LoadFrom(path);

            Type[] allTypes = assembly.GetTypes();

            object dynamicObject = null;

            foreach (Type type in allTypes)
            {
                Console.WriteLine("Type :" + type.Name);
                dynamicObject = assembly.CreateInstance(type.FullName);

                MethodInfo[] allmethods = type.GetMethods(BindingFlags.Public| BindingFlags.Instance | BindingFlags.DeclaredOnly);

                foreach (MethodInfo method in allmethods)
                {
                    Console.Write(method.ReturnType + " " +
                        method.Name + "(" );

                    ParameterInfo[] allParams = method.GetParameters();


                    foreach (ParameterInfo param in allParams)
                    {

                        Console.Write(param.ParameterType.ToString() +
                            " " + param.Name + " " );
                    }

                    Console.WriteLine(")");
                    Console.WriteLine();
                    object[] allParas = new object[] { 10, 20 };

                    object result = type.InvokeMember(method.Name,
                                         BindingFlags.Public |
                                         BindingFlags.Instance |
                                         BindingFlags.InvokeMethod,
                                         null, dynamicObject, allParas);
                    Console.WriteLine("Result of   " + method.Name +
                       "   executed = " + result.ToString() );

 
                }

            

            }

            Console.ReadLine();


        }
    }
}

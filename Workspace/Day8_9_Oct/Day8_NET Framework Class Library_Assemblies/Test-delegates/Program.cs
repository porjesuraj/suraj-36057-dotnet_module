﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
// can have class,delegate
namespace Test_delegates
{ // signature for funmction pointer 
    delegate int mydelegate(int p, int q);
    delegate void stringDelegate(string s);


    class Program
    {
        static void Main(string[] args)
        {
           // mydelegate pointer = new mydelegate(Add);
            mydelegate pointer = new mydelegate(Sub);
            stringDelegate strings = new stringDelegate(SayHello);

            int result = pointer(10, 20);
            Console.WriteLine("result = " + result.ToString());

            strings("hello ");
            Console.ReadLine();
        }

        public static int Add(int x,int y)
        {
            return x + y;

            
        }
        public static int Sub(int x, int y)
        {
            return x - y;


        }
        public static void SayHello(string s)
        {
            
            Console.WriteLine(s);
        }
        // using pointer need to declare unsafe 
       // public static void SomeFunction()
       // {
       //     unsafe
       //     {
       //         int* ptr;
       //     }
       //        
       // }
    }
}
